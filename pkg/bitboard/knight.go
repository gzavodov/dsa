package bitboard

// Knight Шахматный конь
type Knight struct {
	BaseFigure
}

func NewKnight() *Knight {
	return &Knight{BaseFigure{name: "Конь", strategy: NewKnightMoveStrategy()}}
}
