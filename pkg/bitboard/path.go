package bitboard

const (
	ChessboardTopEdge    = 0xff00000000000000
	ChessboardLeftEdge   = 0x101010101010101
	ChessboardBottomEdge = 0xff
	ChessboardRightEdge  = 0x8080808080808080
)

const (
	DirectionTop    = 0x1
	DirectionLeft   = 0x2
	DirectionBottom = 0x4
	DirectionRight  = 0x8
)

type Path struct {
	Position   uint64
	Directions int
}

func (p Path) CheckDirection(direction int) bool {
	return (p.Directions & direction) == direction
}

func (p Path) IsDirectionAvailable(direction int) bool {
	switch direction {
	case DirectionTop:
		return (p.Position & ChessboardTopEdge) == 0
	case DirectionLeft:
		return (p.Position & ChessboardLeftEdge) == 0
	case DirectionBottom:
		return (p.Position & ChessboardBottomEdge) == 0
	case DirectionRight:
		return (p.Position & ChessboardRightEdge) == 0
	}

	return false
}

const (
	MaxRowIndex    = 7
	MaxColumnIndex = 7
)

var rowMasks = []uint64{
	0xff,
	0xff00,
	0xff0000,
	0xff000000,
	0xff00000000,
	0xff0000000000,
	0xff000000000000,
	0xff00000000000000,
}

var columnMasks = []uint64{
	0x101010101010101,
	0x202020202020202,
	0x404040404040404,
	0x808080808080808,
	0x1010101010101010,
	0x2020202020202020,
	0x4040404040404040,
	0x8080808080808080,
}

func (p Path) GetRowIndex(position uint64) int {
	for i := 0; i <= MaxRowIndex; i++ {
		if (position & rowMasks[i]) > 0 {
			return i
		}
	}

	return 0
}

func (p Path) GetColumnIndex(position uint64) int {
	for i := 0; i <= MaxColumnIndex; i++ {
		if (position & columnMasks[i]) > 0 {
			return i
		}
	}

	return 0
}

func (p Path) GetDistanceToEdge(direction int) int {
	switch direction {
	case DirectionTop:
		return MaxRowIndex - p.GetRowIndex(p.Position)
	case DirectionLeft:
		return p.GetColumnIndex(p.Position)
	case DirectionBottom:
		return p.GetRowIndex(p.Position)
	case DirectionRight:
		return MaxColumnIndex - p.GetColumnIndex(p.Position)
	}

	return 0
}

func (p Path) Max(x, y uint64) uint64 {
	if x < y {
		return y
	}
	return x
}

func (p Path) Min(x, y uint64) uint64 {
	if x > y {
		return y
	}
	return x
}
