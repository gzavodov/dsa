package bitboard

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestQueenMovesCalculation(t *testing.T) {
	figure := NewQueen()
	runner := test.Runner{
		Caption: fmt.Sprintf("Вычисление возможных ходов шахматной фигуры \"%s\"", figure.GetName()),
		Task: &PositionCalculationTask{
			Figure:            figure,
			DataDirectoryPath: "../../testify/3.Bits/5.Bitboard - Ферзь",
		},
	}

	runner.TestAll(t)
}
