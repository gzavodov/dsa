package bitboard

// Bishop Шахматный слон
type Bishop struct {
	BaseFigure
}

func NewBishop() *Bishop {
	return &Bishop{BaseFigure{name: "Слон", strategy: NewBishopMoveStrategy()}}
}
