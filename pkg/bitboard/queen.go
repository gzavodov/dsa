package bitboard

// Queen Шахматный ферзь
type Queen struct {
	BaseFigure
}

func NewQueen() *Queen {
	return &Queen{BaseFigure{name: "Ферзь", strategy: NewQueenMoveStrategy()}}
}
