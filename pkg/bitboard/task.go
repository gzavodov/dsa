package bitboard

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"math/bits"
	"strconv"
	"time"
)

type PositionCalculationTask struct {
	Figure            Figure
	DataDirectoryPath string
}

func (t *PositionCalculationTask) Run(in string) *test.Result {
	index, err := strconv.ParseInt(in, 10, 32)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(err)
	}

	start := time.Now()

	pos := uint64(1) << index
	moves := t.Figure.GetMoveStrategy().CalculatePossiblePositions(
		Path{
			Position:   pos,
			Directions: DirectionTop | DirectionLeft | DirectionBottom | DirectionRight,
		},
	)

	finish := time.Now()

	return test.NewResult().
		WithSummary(fmt.Sprintf("Индекс позиции: %d", index)).
		OnComplete(
			start,
			finish,
			fmt.Sprintf(
				"%s\r\n%s",
				strconv.FormatInt(int64(bits.OnesCount64(moves)), 10),
				strconv.FormatUint(moves, 10),
			),
		)
}

func (t *PositionCalculationTask) GetDataSource() test.DataSource {
	return &test.FileDataSource{DataDirectoryPath: t.DataDirectoryPath}
}
