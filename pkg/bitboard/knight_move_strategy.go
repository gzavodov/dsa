package bitboard

type KnightMoveStrategy struct {
}

func NewKnightMoveStrategy() *KnightMoveStrategy {
	return &KnightMoveStrategy{}
}

func (s KnightMoveStrategy) CalculatePossiblePositions(path Path) uint64 {
	result := uint64(0)

	canMoveToTop := path.CheckDirection(DirectionTop) && path.IsDirectionAvailable(DirectionTop)
	canMoveToLeft := path.CheckDirection(DirectionLeft) && path.IsDirectionAvailable(DirectionLeft)
	canMoveToBottom := path.CheckDirection(DirectionBottom) && path.IsDirectionAvailable(DirectionBottom)
	canMoveToRight := path.CheckDirection(DirectionRight) && path.IsDirectionAvailable(DirectionRight)

	maxTopDistance := uint64(path.GetDistanceToEdge(DirectionTop))
	maxLeftDistance := uint64(path.GetDistanceToEdge(DirectionLeft))
	maxBottomDistance := uint64(path.GetDistanceToEdge(DirectionBottom))
	maxRightDistance := uint64(path.GetDistanceToEdge(DirectionRight))

	if canMoveToTop {
		if canMoveToLeft {
			if maxLeftDistance >= 2 {
				result |= path.Position << 6
			}

			if maxTopDistance >= 2 {
				result |= path.Position << 15
			}
		}

		if canMoveToRight {
			if maxRightDistance >= 2 {
				result |= path.Position << 10
			}

			if maxTopDistance >= 2 {
				result |= path.Position << 17
			}
		}
	}

	if canMoveToBottom {
		if canMoveToLeft {
			if maxLeftDistance >= 2 {
				result |= path.Position >> 10
			}

			if maxBottomDistance >= 2 {
				result |= path.Position >> 17
			}
		}

		if canMoveToRight {
			if maxRightDistance >= 2 {
				result |= path.Position >> 6
			}

			if maxBottomDistance >= 2 {
				result |= path.Position >> 15
			}
		}
	}

	return result
}
