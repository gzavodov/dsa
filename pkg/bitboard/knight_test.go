package bitboard

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestKnightMovesCalculation(t *testing.T) {
	figure := NewKnight()
	runner := test.Runner{
		Caption: fmt.Sprintf("Вычисление возможных ходов шахматной фигуры \"%s\"", figure.GetName()),
		Task: &PositionCalculationTask{
			Figure:            figure,
			DataDirectoryPath: "../../testify/3.Bits/2.Bitboard - Конь",
		},
	}

	runner.TestAll(t)
}
