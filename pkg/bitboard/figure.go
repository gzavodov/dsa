package bitboard

// Figure Шахматная фигура
type Figure interface {
	GetName() string

	GetPosition() uint64
	SetPosition(uint64)

	GetMoveStrategy() MoveStrategy
}
