package bitboard

// King Шахматный король
type King struct {
	BaseFigure
}

func NewKing() *King {
	return &King{BaseFigure{name: "Король", strategy: NewKingMoveStrategy()}}
}
