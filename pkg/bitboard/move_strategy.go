package bitboard

type MoveStrategy interface {
	CalculatePossiblePositions(path Path) uint64
}
