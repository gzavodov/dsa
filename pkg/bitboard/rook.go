package bitboard

// Rook Шахматная ладья
type Rook struct {
	BaseFigure
}

func NewRook() *Rook {
	return &Rook{BaseFigure{name: "Ладья", strategy: NewRookMoveStrategy()}}
}
