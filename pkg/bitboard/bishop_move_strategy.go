package bitboard

type BishopMoveStrategy struct {
}

func NewBishopMoveStrategy() *BishopMoveStrategy {
	return &BishopMoveStrategy{}
}

func (s BishopMoveStrategy) CalculatePossiblePositions(path Path) uint64 {
	result := uint64(0)

	canMoveToTop := path.CheckDirection(DirectionTop) && path.IsDirectionAvailable(DirectionTop)
	canMoveToLeft := path.CheckDirection(DirectionLeft) && path.IsDirectionAvailable(DirectionLeft)
	canMoveToBottom := path.CheckDirection(DirectionBottom) && path.IsDirectionAvailable(DirectionBottom)
	canMoveToRight := path.CheckDirection(DirectionRight) && path.IsDirectionAvailable(DirectionRight)

	maxTopDistance := uint64(path.GetDistanceToEdge(DirectionTop))
	maxLeftDistance := uint64(path.GetDistanceToEdge(DirectionLeft))
	maxBottomDistance := uint64(path.GetDistanceToEdge(DirectionBottom))
	maxRightDistance := uint64(path.GetDistanceToEdge(DirectionRight))

	if canMoveToTop {
		if canMoveToLeft {
			pos := path.Position
			for i, distance := uint64(0), path.Min(maxTopDistance, maxLeftDistance); i < distance; i++ {
				pos <<= 7
				result |= pos
			}
		}

		if canMoveToRight {
			pos := path.Position
			for i, distance := uint64(0), path.Min(maxTopDistance, maxRightDistance); i < distance; i++ {
				pos <<= 9
				result |= pos
			}
		}
	}

	if canMoveToBottom {
		if canMoveToLeft {
			pos := path.Position
			for i, distance := uint64(0), path.Min(maxBottomDistance, maxLeftDistance); i < distance; i++ {
				pos >>= 9
				result |= pos
			}
		}

		if canMoveToRight {
			pos := path.Position
			for i, distance := uint64(0), path.Min(maxBottomDistance, maxRightDistance); i < distance; i++ {
				pos >>= 7
				result |= pos
			}
		}
	}

	return result
}
