package bitboard

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestKingMovesCalculation(t *testing.T) {
	figure := NewKing()
	runner := test.Runner{
		Caption: fmt.Sprintf("Вычисление возможных ходов шахматной фигуры \"%s\"", figure.GetName()),
		Task: &PositionCalculationTask{
			Figure:            figure,
			DataDirectoryPath: "../../testify/3.Bits/1.Bitboard - Король",
		},
	}

	runner.TestAll(t)
}
