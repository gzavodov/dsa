package bitboard

type QueenMoveStrategy struct {
	strategies []MoveStrategy
}

func NewQueenMoveStrategy() *QueenMoveStrategy {
	return &QueenMoveStrategy{
		strategies: []MoveStrategy{
			NewBishopMoveStrategy(),
			NewRookMoveStrategy(),
		},
	}
}

func (s QueenMoveStrategy) CalculatePossiblePositions(path Path) uint64 {
	result := uint64(0)
	for _, strategy := range s.strategies {
		result |= strategy.CalculatePossiblePositions(path)
	}

	return result
}
