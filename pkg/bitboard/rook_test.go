package bitboard

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestRookMovesCalculation(t *testing.T) {
	figure := NewRook()
	runner := test.Runner{
		Caption: fmt.Sprintf("Вычисление возможных ходов шахматной фигуры \"%s\"", figure.GetName()),
		Task: &PositionCalculationTask{
			Figure:            figure,
			DataDirectoryPath: "../../testify/3.Bits/3.Bitboard - Ладья",
		},
	}

	runner.TestAll(t)
}
