package bitboard

type KingMoveStrategy struct {
}

func NewKingMoveStrategy() *KingMoveStrategy {
	return &KingMoveStrategy{}
}

func (s KingMoveStrategy) CalculatePossiblePositions(path Path) uint64 {
	result := uint64(0)

	canMoveToTop := path.CheckDirection(DirectionTop) && path.IsDirectionAvailable(DirectionTop)
	canMoveToLeft := path.CheckDirection(DirectionLeft) && path.IsDirectionAvailable(DirectionLeft)
	canMoveToBottom := path.CheckDirection(DirectionBottom) && path.IsDirectionAvailable(DirectionBottom)
	canMoveToRight := path.CheckDirection(DirectionRight) && path.IsDirectionAvailable(DirectionRight)

	if canMoveToTop {
		if canMoveToLeft {
			result |= path.Position << 7
		}

		result |= path.Position << 8

		if canMoveToRight {
			result |= path.Position << 9
		}
	}

	if canMoveToLeft {
		result |= path.Position >> 1
	}

	if canMoveToBottom {
		if canMoveToRight {
			result |= path.Position >> 7
		}

		result |= path.Position >> 8

		if canMoveToLeft {
			result |= path.Position >> 9
		}
	}

	if canMoveToRight {
		result |= path.Position << 1
	}

	return result
}
