package bitboard

type RookMoveStrategy struct {
}

func NewRookMoveStrategy() *RookMoveStrategy {
	return &RookMoveStrategy{}
}

func (s RookMoveStrategy) CalculatePossiblePositions(path Path) uint64 {
	result := uint64(0)

	if path.CheckDirection(DirectionTop) && path.IsDirectionAvailable(DirectionTop) {
		pos := path.Position
		for i, distance := uint64(0), uint64(path.GetDistanceToEdge(DirectionTop)); i < distance; i++ {
			pos <<= 8
			result |= pos
		}
	}

	if path.CheckDirection(DirectionLeft) && path.IsDirectionAvailable(DirectionLeft) {
		pos := path.Position
		for i, distance := uint64(0), uint64(path.GetDistanceToEdge(DirectionLeft)); i < distance; i++ {
			pos >>= 1
			result |= pos
		}
	}

	if path.CheckDirection(DirectionBottom) && path.IsDirectionAvailable(DirectionBottom) {
		pos := path.Position
		for i, distance := uint64(0), uint64(path.GetDistanceToEdge(DirectionBottom)); i < distance; i++ {
			pos >>= 8
			result |= pos
		}
	}

	if path.CheckDirection(DirectionRight) && path.IsDirectionAvailable(DirectionRight) {
		pos := path.Position
		for i, distance := uint64(0), uint64(path.GetDistanceToEdge(DirectionRight)); i < distance; i++ {
			pos <<= 1
			result |= pos
		}
	}

	return result
}
