package bitboard

// BaseFigure Базовая шахматная фигура
type BaseFigure struct {
	name     string
	position uint64
	strategy MoveStrategy
}

func (f BaseFigure) GetName() string {
	return f.name
}

func (f BaseFigure) GetPosition() uint64 {
	return f.position
}

func (f *BaseFigure) SetPosition(pos uint64) {
	f.position = pos
}

func (f *BaseFigure) GetMoveStrategy() MoveStrategy {
	return f.strategy
}
