package queue

import (
	"fmt"
	"strings"
)

// PriorityQueueNode - элемент односвязанного списка очереди с приоритетами.
// Значения хранятся в массиве, который работает по схеме FIFO (очередь).
type PriorityQueueNode struct {
	Priority int
	Next     *PriorityQueueNode
	Items    []interface{}
}

func (n *PriorityQueueNode) PushItem(item interface{}) {
	n.Items = append(n.Items, item)
}

func (n *PriorityQueueNode) ShiftItem() interface{} {
	if len(n.Items) == 0 {
		return nil
	}

	item := n.Items[0]
	n.Items = append(n.Items[:0], n.Items[1:]...)
	return item
}

func (n *PriorityQueueNode) HasItems() bool {
	return len(n.Items) > 0
}

func (n *PriorityQueueNode) String() string {
	builder := strings.Builder{}
	builder.WriteString(fmt.Sprintf("%d", n.Priority))

	builder.WriteString(": [")
	for i, item := range n.Items {
		if i > 0 {
			builder.WriteString(", ")
		}
		builder.WriteString(fmt.Sprintf("%s", item))
	}
	builder.WriteString("]")
	return builder.String()
}

// PriorityQueue - "Наивная" реализация очереди с приоритетами (только для тестов)
type PriorityQueue struct {
	Node *PriorityQueueNode
}

func (q *PriorityQueue) Enqueue(priority int, item interface{}) {
	var prevNode, curNode *PriorityQueueNode

	curNode = q.Node
	for {
		if curNode == nil {
			newNode := &PriorityQueueNode{Priority: priority, Items: []interface{}{item}}
			if prevNode != nil {
				prevNode.Next = newNode
			} else {
				q.Node = newNode
			}

			break
		}

		if curNode.Priority < priority {
			newNode := &PriorityQueueNode{Priority: priority, Items: []interface{}{item}, Next: curNode}
			if prevNode != nil {
				prevNode.Next = newNode
			} else {
				q.Node = newNode
			}

			break
		}

		if curNode.Priority == priority {
			curNode.PushItem(item)
			break
		}

		prevNode = curNode
		curNode = curNode.Next
	}
}

func (q *PriorityQueue) Dequeue() interface{} {
	if q.Node == nil {
		return nil
	}

	result := q.Node.ShiftItem()
	if !q.Node.HasItems() {
		q.Node = q.Node.Next
	}

	return result
}

func (q *PriorityQueue) String() string {
	builder := strings.Builder{}

	node := q.Node
	for {
		if node == nil {
			break
		}

		builder.WriteString(node.String())
		builder.WriteString("\n")

		node = node.Next
	}

	return builder.String()
}