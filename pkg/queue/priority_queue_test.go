package queue

import (
	"math/rand"
	"testing"
	"time"
)

func TestPriorityQueue(t *testing.T) {
	queue := PriorityQueue{}

	limit := 10000

	t.Logf("Тестирование очереди приоритетов. Кол-во эл-тов: %d.\n", limit)
	t.Logf("Создание упорядоченного массива приоритетов для проверки.\n")

	orderedPriorities := make([]int, limit, limit)
	for i := 1; i <= limit; i++ {
		orderedPriorities[i-1] = i
	}

	t.Logf("Создание перемешанного массива приоритетов для заполнения очереди.\n")

	priorities := make([]int, limit, limit)
	copy(priorities, orderedPriorities)

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(priorities), func(i, j int) { priorities[i], priorities[j] = priorities[j], priorities[i] })

	start := time.Now()

	t.Logf("Заполнение очереди.\n")
	for _, priority := range priorities {
		queue.Enqueue(priority, priority)
	}
	finish := time.Now()

	t.Logf("Затраченное время: %d 10^(-9) сек.\n", finish.Sub(start).Nanoseconds())

	// Идём по проверочному массиву приоритетов с конца, получая следующий эл-т очереди. Значения должны совпадать.
	for i := limit - 1; i >= 0; i-- {
		priority := orderedPriorities[i]
		queuePriority := queue.Dequeue().(int)
		if queuePriority != priority {
			t.Errorf("incorrect priority received, suspected: %d, but received: %d", priority, queuePriority)
		}
	}
}
