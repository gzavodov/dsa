package test

type InMemoryDataSource struct {
	Description        string
	Data               []TaskData
	EnableOutcomeCheck bool
}

// GetDescription Получить описание задачи.
func (ds *InMemoryDataSource) GetDescription() string {
	return ds.Description
}

func (ds *InMemoryDataSource) GetTotalCount() int {
	return len(ds.Data)
}

func (ds *InMemoryDataSource) Get(index int) (TaskData, error) {
	if index < 0 || index >= len(ds.Data) {
		return TaskData{}, ErrNotFound
	}

	return ds.Data[index], nil
}

func (ds *InMemoryDataSource) IsResultCheckEnabled() bool {
	return ds.EnableOutcomeCheck
}