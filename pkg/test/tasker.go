package test

type Tasker interface {
	GetDataSource() DataSource
	Run(in string) *Result
}
