package test

import "time"

type Result struct {
	Value      string
	Summary    string
	StartTime  time.Time
	FinishTime time.Time
	Error      error
}

func NewResult() *Result {
	return &Result{}
}

func (r *Result) WithSummary(summary string) *Result {
	r.Summary = summary
	return r
}

func (r *Result) OnStart() *Result {
	r.StartTime = time.Now()
	return r
}

func (r *Result) OnFinish(value string) *Result {
	r.Value = value
	r.FinishTime = time.Now()
	return r
}

func (r *Result) OnError(err error) *Result {
	r.Error = err
	return r
}

func (r *Result) OnComplete(start time.Time, finish time.Time, value string) *Result {
	r.StartTime = start
	r.FinishTime = finish
	r.Value = value
	return r
}

func (r *Result) GetElapsedTime() time.Duration {
	return r.FinishTime.Sub(r.StartTime)
}
