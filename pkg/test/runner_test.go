package test

import (
	"strconv"
	"testing"
	"time"
	"unicode/utf8"
)

// StringLengthTask Вычисление длины строки в символах. Простая задача для проверки тестировщика задач.
type StringLengthTask struct {
}

func (t *StringLengthTask) Run(in string) *Result {
	return NewResult().
		OnComplete(time.Now(), time.Now(), strconv.Itoa(utf8.RuneCountInString(in)))
}

func (t *StringLengthTask) GetDataSource() DataSource {
	return &FileDataSource{DataDirectoryPath: "../../testify/0.String" }
}

func TestRunner(t *testing.T) {
	(&Runner{Task: &StringLengthTask{}}).TestAll(t)
}
