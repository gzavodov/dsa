package test

import (
	"fmt"
	"testing"
)

// Runner Запускает выполнение задачи для всех входных данных в директории задачи и сравнивает полученные результаты с ожидаемыми.
// Файлы входных данных ищутся по шаблону "test.[номер].in".
// Файлы ожидаемых результатов ищутся по шаблону "test.[номер].out".
type Runner struct {
	Task        Tasker
	Caption     string
	HideResults bool
	HideErrors  bool
}

// GetDescription Получить описание задачи.
// Ищем файл problem.txt в директории задачи.
// Если находим, то возвращается его содержание. Если нет, то возвращается пустая строка.
func (r *Runner) GetDescription() string {
	return r.Task.GetDataSource().GetDescription()
}

// Run Запуск выполнения задачи для входных данных из директории задачи.
// После выполнения каждого теста полученный результат сравнивается с ожидаемым.
// При расхождении фактических и ожидаемых результатов возвращается ошибка.
// Параметр limit позволяет задать кол-во запускаемых тестов
// если limit == 0, то будут выполнены все тесты из директории задачи;
// если limit < 0, то будут выполнены все тесты, кроме |limit| последний тестов (т.е. limit = -2 пропустит последние два теста).
func (r *Runner) Run(limit int) []*Result {
	dataSource := r.Task.GetDataSource()

	var results []*Result

	if limit < 0 {
		limit += dataSource.GetTotalCount()
	}

	enableResultCheck := dataSource.IsResultCheckEnabled()

	// Обход директории данных задачи с поиском по номеру теста начиная с 0
	counter := 0
	for {
		if limit > 0 && counter >= limit {
			break
		}

		data, err := dataSource.Get(counter)
		if err != nil {
			if err == ErrNotFound {
				break
			}

			return append(results, NewResult().OnError(err))
		}

		//Прогон теста с обработкой результата
		result := r.Task.Run(data.Income)
		results = append(results, result)

		if result.Error != nil {
			//При выполнении теста возникла ошибка - прерываем выполнение
			break
		}

		if enableResultCheck && result.Value != data.Outcome {
			result.Error = fmt.Errorf(
				"расхождение с ожидаемым результатом в тесте №%d, входные данные: \"%s\", что ожидается получить: \"%s\", что получено: \"%s\"",
				counter,
				data.Income,
				data.Outcome,
				result.Value,
			)
		}

		counter++
	}

	return results
}

// Test Тестирование задачи на входных данных из директории задачи.
// Запускается метод Run. Полученные результаты выводятся в лог.
// Параметр limit позволяет задать кол-во запускаемых тестов
// если limit 0 <= 0, то будут выполнены все тесты из директории задачи.
func (r *Runner) Test(t *testing.T, limit int) {
	description := r.GetDescription()
	if description != "" {
		t.Log("==================================================================")
		t.Logf("%s\n", r.GetDescription())
	}

	if r.Caption != "" {
		t.Log("==================================================================")
		t.Log(r.Caption)
	}

	if description != "" || r.Caption != "" {
		t.Log("==================================================================")
	}

	results := r.Run(limit)

	isSuccessful := true
	for i, result := range results {

		if len(result.Summary) > 0 {
			t.Logf("Тест №%d\t(%s)", i, result.Summary)
		} else {
			t.Logf("Тест №%d", i)
		}

		if !r.HideResults {
			t.Logf("Результат:\t%s\n", result.Value)
		}

		if result.Error != nil {
			if r.HideErrors {
				t.Log("Статус:\t\tНе пройден")
				t.Logf("Время:\t\t%d 10^(-9) сек.\n", result.GetElapsedTime().Nanoseconds())
				t.Error("Тест не пройден")
			} else {
				t.Log("Статус:\t\tОшибка")
				t.Error(result.Error)
			}

			if isSuccessful {
				isSuccessful = false
			}
		} else {
			t.Log("Статус:\t\tПройден")
			t.Logf("Время:\t\t%d 10^(-9) сек.\n", result.GetElapsedTime().Nanoseconds())
		}

		t.Log("==================================================================")
	}

	if isSuccessful {
		t.Log("Тест успешно пройден")
	} else {
		t.Log("Тест не пройден")
	}
}

// TestAll Тестирование задачи на всех входных данных из директории задачи.
// Запускается метод Run. Полученные результаты выводятся в лог.
func (r *Runner) TestAll(t *testing.T) {
	r.Test(t, 0)
}
