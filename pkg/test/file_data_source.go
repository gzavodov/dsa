package test

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

type FileDataSource struct {
	DataDirectoryPath string
}

// GetDescription Получить описание задачи.
// Ищем файл problem.txt в директории задачи.
// Если находим, то возвращается его содержание. Если нет, то возвращается пустая строка.
func (ds *FileDataSource) GetDescription() string {
	descriptionBytes, err := ioutil.ReadFile(
		filepath.Join(ds.DataDirectoryPath, "problem.txt"),
	)

	if err != nil {
		return ""
	}

	return string(descriptionBytes)
}

func (ds *FileDataSource) GetTotalCount() int {
	// Обход директории данных задачи и подсчёт общего кол-ва тестов.
	totalCount := 0
	for {
		_, err := os.Stat(filepath.Join(ds.DataDirectoryPath, fmt.Sprintf("test.%d.in", totalCount)))
		if os.IsNotExist(err) {
			break
		}

		totalCount++
	}

	return totalCount
}

func (ds *FileDataSource) Get(index int) (TaskData, error) {
	//Входные данные теста
	inBytes, err := ioutil.ReadFile(filepath.Join(ds.DataDirectoryPath, fmt.Sprintf("test.%d.in", index)))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return TaskData{}, ErrNotFound
		}

		return TaskData{}, err
	}

	in := strings.TrimSpace(string(inBytes))

	//Ожидаемый результат теста
	outBytes, err := ioutil.ReadFile(filepath.Join(ds.DataDirectoryPath, fmt.Sprintf("test.%d.out", index)))
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return TaskData{}, ErrNotFound
		}

		return TaskData{}, err
	}

	out := strings.TrimSpace(string(outBytes))

	return TaskData{Income: in, Outcome: out}, nil
}

func (ds *FileDataSource) IsResultCheckEnabled() bool {
	return true
}