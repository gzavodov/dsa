package test

import "errors"

var ErrNotFound = errors.New("not found")

type DataSource interface {
	GetDescription() string
	GetTotalCount() int
	Get(index int) (TaskData, error)
	IsResultCheckEnabled() bool
}
