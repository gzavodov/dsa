package iteration

import (
	"gitlab.com/gzavodov/dsa/pkg/power/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestIterationAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация возведения числа в степень итеративным умножением на само себя",
		Task: &common.CalculationTask{
			Algorithm:         NewAlgorithm(),
			DataDirectoryPath: "../../../testify/4.Power",
		},
	}

	// Последние тесты (1.0000000001 ^ 10000000000, 1.000000001 ^ 1000000000) отключены из-за очень долгого времени работы
	//runner.TestAll(t)
	runner.Test(t, -2)
}
