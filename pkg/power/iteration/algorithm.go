package iteration

import "math/big"

type Algorithm struct{}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a Algorithm) Calculate(x float64, y int64) float64 {
	bigX := big.NewFloat(0).SetPrec(256).SetFloat64(x)
	bigResult := big.NewFloat(0).SetPrec(256).SetFloat64(1.0)

	for i := int64(0); i < y; i++ {
		bigResult.Mul(bigResult, bigX)
	}

	result, _ := bigResult.Float64()
	return result
}
