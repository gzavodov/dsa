package common

type Algorithm interface {
	Calculate(x float64, y int64) float64
}
