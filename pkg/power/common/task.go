package common

import (
	"errors"
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"math"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type CalculationTask struct {
	Algorithm         Algorithm
	DataDirectoryPath string
}

var incomeRegex = regexp.MustCompile(`^\s*(\d+(?:\.\d+)?)\s+(\d+)\s*$`)

func (t *CalculationTask) Run(in string) *test.Result {
	results := incomeRegex.FindStringSubmatch(in)
	if len(results) < 3 {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(errors.New("не удалось разобрать входную строку"))
	}

	x, err := strconv.ParseFloat(results[1], 10)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", results[1])).
			OnError(err)
	}

	y, err := strconv.ParseInt(results[2], 10, 64)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", results[2])).
			OnError(err)
	}

	start := time.Now()
	value := t.Algorithm.Calculate(x, y)
	finish := time.Now()

	// Здесь нужно подстроить формат вывода под чудной формат проверочных результатов.
	// * результаты без десятичной части выводим с 1 десятичным знаком (1.0);
	// * результаты с десятичной частью выводим с 11 десятичными знаками, но нули справа обрезаются (2.71814592682);

	var str string
	if math.Trunc(value) == value {
		str = fmt.Sprintf("%.1f", value)
	} else {
		factor := math.Pow(10, float64(11))
		roundedValue := math.Round(value * factor) / factor
		str = strings.TrimRight(fmt.Sprintf("%.11f", roundedValue), "0")
	}

	return test.NewResult().
		WithSummary(
			fmt.Sprintf(
				"%s ^ %s",
				strconv.FormatFloat(x, 'f', -1, 64),
				strconv.FormatInt(y, 10),
			),
		).
		OnComplete(start, finish, str)
}

func (t *CalculationTask) GetDataSource() test.DataSource {
	return &test.FileDataSource{DataDirectoryPath: t.DataDirectoryPath}
}
