package binary

import (
	"gitlab.com/gzavodov/dsa/pkg/power/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestBinaryAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация возведения числа в степень через двоичное разложение показателя степени",
		Task: &common.CalculationTask{
			Algorithm:         NewAlgorithm(),
			DataDirectoryPath: "../../../testify/4.Power",
		},
	}

	runner.TestAll(t)
}
