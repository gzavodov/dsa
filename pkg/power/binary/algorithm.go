package binary

import "math/big"

type Algorithm struct{}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a Algorithm) Calculate(x float64, y int64) float64 {
	if y == 0 {
		return 1.0
	}

	bigX := big.NewFloat(0).SetPrec(256).SetFloat64(x)
	bigResult := big.NewFloat(0).SetPrec(256).SetFloat64(1)
	for {
		if y == 0 {
			break
		}

		if y & 1 != 0 {
			bigResult.Mul(bigResult, bigX)
		}

		bigX.Mul(bigX, bigX)
		y >>= 1
	}

	result, _ := bigResult.Float64()
	return result
}

func (a Algorithm) CalculateBigFloat(x *big.Float, y int64, prec uint) *big.Float {
	if y == 0 {
		return big.NewFloat(1.0)
	}

	bigX := big.NewFloat(0).SetPrec(prec).Set(x)
	result := big.NewFloat(0).SetPrec(prec).SetFloat64(1)
	for {
		if y == 0 {
			break
		}

		if y & 1 != 0 {
			result.Mul(result, bigX)
		}

		bigX.Mul(bigX, bigX)
		y >>= 1
	}

	return result
}