package pow_of_2

import "math/big"

type Algorithm struct{}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a Algorithm) Calculate(x float64, y int64) float64 {
	if y == 0 {
		return 1.0
	}

	bigX := big.NewFloat(0).SetPrec(256).SetFloat64(x)
	bigResult := big.NewFloat(0).SetPrec(256).SetFloat64(x)
	k := int64(1)
	for i := int64(0); i < y; i++ {
		if k*2 > y {
			break
		}

		bigResult.Mul(bigResult, bigResult)
		k *= 2
	}

	for i, n := int64(0), y-k; i < n; i++ {
		bigResult.Mul(bigResult, bigX)
		k += 1
	}

	result, _ := bigResult.Float64()
	return result
}

func (a Algorithm) CalculateBigFloat(x *big.Float, y int64, prec uint) *big.Float {
	if y == 0 {
		return big.NewFloat(1.0)
	}

	result := big.NewFloat(0).SetPrec(prec).Set(x)
	k := int64(1)
	for i := int64(0); i < y; i++ {
		if k*2 > y {
			break
		}

		result.Mul(result, result)
		k *= 2
	}

	for i, n := int64(0), y-k; i < n; i++ {
		result.Mul(result, x)
		k += 1
	}

	return result
}