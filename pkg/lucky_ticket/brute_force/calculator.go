package brute_force

import (
	"gitlab.com/gzavodov/dsa/pkg/lucky_ticket/common"
	"math"
)

type Calculator struct {
}

func NewCalculator() *Calculator {
	return &Calculator{}
}

func (c *Calculator) Calculate(length int64) *common.CalculationResult {
	ceiling := int64(math.Pow(10, float64(length)))
	result := common.NewCalculationResult(length).OnStart()

	for i := int64(0); i < ceiling; i++ {
		firstSum, err := common.GetDigitsSum(i)
		if err != nil {
			return result.OnError(err)
		}

		for j := int64(0); j < ceiling; j++ {
			secondSum, err := common.GetDigitsSum(j)
			if err != nil {
				return result.OnError(err)
			}

			if firstSum == secondSum {
				result.Value++
			}
		}
	}

	return result.OnFinish()
}
