package brute_force

import (
	"gitlab.com/gzavodov/dsa/pkg/lucky_ticket/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestBruteForceAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация подсчёта счастливых билетов перебором",
		Task: &common.CalculationTask{
			Calculator:        NewCalculator(),
			DataDirectoryPath: "../../../testify/1.Tickets",
		},
	}

	// Ограничиваем кол-во тестов четырьмя первыми из-за медленной работы алгоритма
	runner.Test(t, 4)
}
