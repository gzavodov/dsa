package common

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"strconv"
)

type CalculationTask struct {
	Calculator        Calculator
	DataDirectoryPath string
}

func (t *CalculationTask) Run(in string) *test.Result {
	length, err := strconv.ParseInt(in, 10, 32)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(err)
	}

	return t.Calculator.Calculate(length).ToTestResult()
}

func (t *CalculationTask) GetDataSource() test.DataSource {
	return &test.FileDataSource{DataDirectoryPath: t.DataDirectoryPath}
}
