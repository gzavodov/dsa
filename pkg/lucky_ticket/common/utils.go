package common

import "strconv"

// GetNumericRoot Вычислить числовой корень числа
// Для ускорения заменяем суммирование цифр вычислением остатка от деления на 9.
// Например, сумма цифр числа 295:  295 => 2 + 9 + 5 = 16 => 1 + 6 = 7
func GetNumericRoot(value int64) int64 {
	if value == 0 {
		return 0
	}

	result := value % 9
	if result == 0 {
		//Число делится на 9 без остатка, значит числовой корень числа 9 (например: 9, 18, 27, 36, 45, 54, 63, 72, 81, 90, 99 и т.д)
		return 9
	}

	return result
}

var digitsSums = make(map[int64]int64)

// GetDigitsSum Получить сумму цифр в числе
// В отличии от числового корня делается только одна итерация сложения
// Например, сумма цифр числа 295 => 2 + 9 + 5 = 16
func GetDigitsSum(value int64) (int64, error) {
	result, ok := digitsSums[value]
	if ok {
		return result, nil
	}

	str := strconv.FormatInt(value, 10)
	runes := []rune(str)
	for _, r := range runes {
		num, err := strconv.ParseInt(string(r), 10, 32)
		if err != nil {
			return 0, err
		}

		result += num
	}

	digitsSums[value] = result

	return result, nil
}
