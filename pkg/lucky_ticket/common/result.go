package common

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"strconv"
	"time"
)

type CalculationResult struct {
	Value      int64
	Length     int64
	StartTime  time.Time
	FinishTime time.Time

	Error error
}

func NewCalculationResult(length int64) *CalculationResult {
	return &CalculationResult{Length: length}
}

func (r *CalculationResult) OnStart() *CalculationResult {
	r.StartTime = time.Now()
	return r
}

func (r *CalculationResult) OnFinish() *CalculationResult {
	r.FinishTime = time.Now()
	return r
}

func (r *CalculationResult) OnError(err error) *CalculationResult {
	r.Error = err
	return r
}

func (r *CalculationResult) GetElapsedTime() time.Duration {
	return r.FinishTime.Sub(r.StartTime)
}

func (r *CalculationResult) ToTestResult() *test.Result {
	summary := fmt.Sprintf("знаков в билете: %d", 2*r.Length)

	if r.Error != nil {
		return test.NewResult().
			WithSummary(summary).
			OnError(r.Error)
	}

	return test.NewResult().
		WithSummary(summary).
		OnComplete(r.StartTime, r.FinishTime, strconv.FormatInt(r.Value, 10))
}
