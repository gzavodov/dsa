package common

type Calculator interface {
	Calculate(length int64) *CalculationResult
}
