package dynamic_table

import (
	"gitlab.com/gzavodov/dsa/pkg/lucky_ticket/common"
)

type Calculator struct {
}

func NewCalculator() *Calculator {
	return &Calculator{}
}

func (c *Calculator) Calculate(length int64) *common.CalculationResult {
	result := common.NewCalculationResult(length).OnStart()
	if length > 0 {
		var table []int64
		for i := int64(0); i < length; i++ {
			table = c.ExpandTable(table)
		}

		for i := 0; i < len(table); i++ {
			result.Value += table[i] * table[i]
		}
	}

	return result.OnFinish()
}

func (c *Calculator) ExpandTable(table []int64) []int64 {
	length := len(table)
	if length == 0 {
		newTable := make([]int64, 10, 10)
		for i := 0; i < 10; i++ {
			newTable[i] = 1
		}

		return newTable
	}

	maxIndex := len(table) - 1

	newLength := length + 9
	newTable := make([]int64, newLength, newLength)
	newMaxIndex := newLength - 1

	for i := 0; i <= newMaxIndex; i++ {
		var value int64
		for j := 9; j >= 0; j-- {
			index := i - j
			if index >= 0 && index <= maxIndex {
				value += table[index]
			}
		}
		newTable[i] = value
	}

	return newTable
}
