package dynamic_table

import (
	"gitlab.com/gzavodov/dsa/pkg/lucky_ticket/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestDynamicTableAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация подсчёта счастливых билетов с помощью динамических таблиц",
		Task: &common.CalculationTask{
			Calculator:        NewCalculator(),
			DataDirectoryPath: "../../../testify/1.Tickets",
		},
	}

	runner.TestAll(t)
}
