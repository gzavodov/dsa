package finite_automation

type Matcher struct {
}

// preparePatternPrefixes возвращает π-массив префиксных расстояний для заданного паттерна
func (m *Matcher) preparePatternTransitions(pattern string) map[int32][]int32 {
	length := len(pattern)
	results := make(map[int32][]int32)
	chars := make([]int32, 0, length)
	for _, c := range pattern {
		if _, ok := results[c]; ok {
			continue
		}

		chars = append(chars, c)
		results[c] = make([]int32, length)
	}

	for i := 0; i < length; i++ {
		for j := 0; j < len(chars); j++ {
			c := chars[j]

			if c == int32(pattern[i]) {
				results[c][i] = int32(i + 1)
			}
		}
	}

	return results
}
