package kmp

import (
	"reflect"
	"testing"
)

func TestMatcher_PreparePatternPrefixes(t *testing.T) {
	testCases := []struct {
		Pattern  string
		Expected []int
	}{
		{
			Pattern:  "abbaabbab",
			Expected: []int{0, 0, 0, 1, 1, 2, 3, 4, 2},
		},
		{
			Pattern:  "ababaca",
			Expected: []int{0, 0, 1, 2, 3, 0, 1},
		},
		{
			Pattern:  "abcabcd",
			Expected: []int{0, 0, 0, 1, 2, 3, 0},
		},
		{
			Pattern:  "aabaabaaaabaabaaab",
			Expected: []int{0, 1, 0, 1, 2, 3, 4, 5, 2, 2, 3, 4, 5, 6, 7, 8, 9, 3},
		},
		{
			Pattern:  "abbaabba",
			Expected: []int{0, 0, 0, 1, 1, 2, 3, 4},
		},
	}

	m := Matcher{}
	for _, testCase := range testCases {
		received := m.preparePatternPrefixes(testCase.Pattern)
		if !reflect.DeepEqual(received, testCase.Expected) {
			t.Errorf(
				"search pattern '%s' expected prefixes: %v, but received: %v\n",
				testCase.Pattern,
				testCase.Expected, received,
			)
		}
	}
}

func TestMatcher_FindIndex(t *testing.T) {
	testCases := []struct {
		Pattern  string
		Text     string
		Expected int
	}{
		{
			Pattern:  "cat",
			Text:     "one cat two cats in the yard",
			Expected: 4,
		},
		{
			Pattern:  "ababaca",
			Text:     "bacbababaabcbab",
			Expected: -1,
		},
		{
			Pattern:  "ababaca",
			Text:     "bacbababaabcbabababacaba",
			Expected: 15,
		},
	}

	m := Matcher{}
	for _, testCase := range testCases {
		received := m.FindIndex(testCase.Pattern, testCase.Text)
		if received != testCase.Expected {
			t.Errorf(
				"search pattern '%s' in %s expected index: %d, but received: %d\n",
				testCase.Pattern,
				testCase.Text,
				testCase.Expected, received,
			)
		}
	}
}
