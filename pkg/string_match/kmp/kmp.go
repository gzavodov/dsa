package kmp

type Matcher struct {
}

func (m *Matcher) FindIndex(pattern string, text string) int {
	patternLength := len(pattern)
	textLength := len(text)

	// готовим π-массив префиксных расстояний
	pi := m.preparePatternPrefixes(pattern)

	var (
		i = 0
		j = 0
	)

	for i < textLength {
		if pattern[j] == text[i] {
			j++

			if j == patternLength {
				return i - patternLength + 1
			}

			i++
		} else if j > 0 {
			j = pi[j-1]
		} else {
			i++
		}
	}

	return -1
}

// preparePatternPrefixes возвращает π-массив префиксных расстояний для заданного паттерна
func (m *Matcher) preparePatternPrefixes(pattern string) []int {
	length := len(pattern)
	results := make([]int, length)

	var (
		i = 1
		j = 0
	)

	for i < length {
		if pattern[i] == pattern[j] {
			results[i] = j + 1

			i++
			j++
		} else if j > 0 {
			j = results[j-1]
		} else {
			//results[i] = 0
			i++
		}
	}

	return results
}
