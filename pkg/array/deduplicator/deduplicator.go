package deduplicator

import (
	"math/rand"
	"time"
)

type Number interface {
	int8 | int16 | int32 | int64 | uint8 | uint32 | uint16 | uint64 | float32 | float64
}

// Deduplicate - "сворачивает" слайс, удаляя найденные дубликаты.
// Параметр numbers - исходный слайс
// Параметр limit - максимальный размер результирующего слайса.
func Deduplicate[T Number](numbers []T, limit int) []T {
	if len(numbers) < 2 {
		return numbers
	}

	var (
		currentIdx int
		swapIdx    int
	)

	for currentIdx = 1; currentIdx < len(numbers) && currentIdx < limit; currentIdx++ {
		if numbers[currentIdx] > numbers[currentIdx-1] {
			continue
		}

		isFound := false
		if swapIdx < currentIdx {
			swapIdx = currentIdx
		}

		swapIdx, isFound = findNextNumberIndex(numbers, numbers[currentIdx-1], swapIdx+1)
		if !isFound {
			break
		}

		numbers[currentIdx], numbers[swapIdx] = numbers[swapIdx], numbers[currentIdx]
	}

	if currentIdx < len(numbers) {
		var defaultValue T
		for i := currentIdx; i < len(numbers); i++ {
			numbers[i] = defaultValue
		}

		numbers = numbers[:currentIdx]
	}

	return numbers
}

// findNextNumberIndex - возвращает индекс числа, значение которого больше указанного (number).
// Поиск начинается с указанного индекса (startFrom).
func findNextNumberIndex[T Number](numbers []T, number T, startFrom int) (int, bool) {
	for i := startFrom; i < len(numbers); i++ {
		if numbers[i] > number {
			return i, true
		}
	}

	return 0, false
}

// Generate - создаёт слайс отсортированных по возрастанию чисел с дубликатами.
// Параметр length - нужная длина слайса.
// Параметр limit - максимальное кол-во дубликатов для каждого числа.
// Например, если limit = 2, то каждое значение будет встречаться в результирующем слайсе от 0 до 2 раз.
func Generate[T Number](length int, limit int) ([]T, []T) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	duplicates := make([]T, 0, length)
	sequence := make([]T, 0, length)
	var num T
	for len(duplicates) < length {
		count := r.Intn(limit + 1)
		if count > length-len(duplicates) {
			count = length - len(duplicates)
		}

		if count > 0 {
			sequence = append(sequence, num)
			for i := 0; i < count; i++ {
				duplicates = append(duplicates, num)
			}
			num++
		}
	}

	return duplicates, sequence
}
