package deduplicator

import (
	"errors"
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestDeduplicate(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация дедупликации массива отсортированных чисел",
		Task: &Task{
			DataDirectoryPath: "../../../testify/Deduplication",
		},
	}

	runner.TestAll(t)
}

type Task struct {
	DataDirectoryPath string
}

func (t *Task) GetDataSource() test.DataSource {
	return &test.FileDataSource{DataDirectoryPath: t.DataDirectoryPath}
}

func (t *Task) Run(in string) *test.Result {
	parts := strings.SplitN(in, "\r\n", 3)
	if len(parts) < 3 {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(errors.New("не удалось разобрать входную строку"))
	}

	limit, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", parts[1])).
			OnError(errors.New("не удалось конвертировать в число"))
	}

	incomeSlugs := strings.Split(parts[2], " ")
	length := len(incomeSlugs)
	numbers := make([]int64, length)

	for i, slug := range incomeSlugs {
		num, err := strconv.ParseInt(slug, 10, 64)
		if err != nil {
			return test.NewResult().
				WithSummary(fmt.Sprintf("входная строка: \"%s\"", slug)).
				OnError(errors.New("не удалось конвертировать в число"))
		}

		numbers[i] = num

	}

	start := time.Now()
	numbers = Deduplicate[int64](numbers, int(limit))
	finish := time.Now()

	outcomeSlugs := make([]string, len(numbers))
	for i, number := range numbers {
		outcomeSlugs[i] = strconv.FormatInt(number, 10)
	}

	return test.NewResult().
		WithSummary(
			fmt.Sprintf("Исходное кол-во эл-тов: %d, ожидаемое итоговое кол-во эл-тов: %d", length, limit),
		).
		OnComplete(start, finish, strings.Join(outcomeSlugs, " "))
}
