### Алгоритмы поиска кол-ва простых чисел до N макс

В первой колонке указаны операции. В следующий колонках время затраченное на вычисление соответствующим алгоритмом.
Время затраченное на вычисление указано в наносекундах (наносекунда = 10^-9 секунды)

| N макс        | Перебор A (нс) | Перебор B (нс) |  Перебор C (нс) | Перебор D (нс) | Перебор E (нс) | Перебор F (нс) | Решето Эратосфена O(N log log N) | Решето Эратосфена O(N) |
|:--------------|---------------:|---------------:|----------------:|---------------:|---------------:|---------------:|---------------------------------:|-----------------------:|
| 10            |            579 |            320 |             331 |            245 |            347 |            319 |                              246 |                    238 |
| 1             |             63 |             57 |              70 |             57 |             54 |             57 |                               57 |                     73 |
| 2             |             74 |             79 |              70 |             59 |             61 |             80 |                              104 |                    121 |
| 3             |             92 |             74 |              71 |             66 |             99 |             76 |                               92 |                    107 |
| 4             |            134 |             99 |              81 |             81 |            112 |             87 |                               97 |                    122 |
| 5             |            152 |            113 |              88 |             79 |            139 |            107 |                              115 |                    125 |
| 100           |         34 170 |          7 970 |           4 891 |          2 526 |          1 979 |          1 765 |                              439 |                    705 |
| 1000          |      3 535 844 |        563 512 |         409 939 |         39 782 |         31 799 |         24 904 |                            2 920 |                  7 621 |
| 10 000        |    320 571 074 |     37 550 535 |      19 208 237 |        802 520 |        780 986 |        424 552 |                           28 795 |                 61 976 |
| 100 000       | 31 368 141 903 |  2 831 146 785 |   1 397 300 506 |     18 440 579 |     11 323 793 |      5 947 743 |                          264 046 |               5 433 97 |
| 1000 000      |              - |              - | 11 846 848 5240 |   4 270 503 85 |    252 865 539 |    116 826 382 |                        3 096 811 |              44 104 86 |
| 10 000 000    |              - |              - |               - | 10 705 531 616 |  6 157 761 552 |  2 173 377 026 |                       37 699 797 |             49 795 693 |
| 100 000 000   |              - |              - |               - |              - |              - | 47 482 449 112 |                      675 465 945 |            619 350 404 |
| 1 000 000 000 |              - |              - |               - |              - |              - |              - |                    7 628 4308 29 |          6 210 157 637 |
| 123 456 789   |              - |              - |               - |              - |              - |              - |                      854 675 186 |           6 07 132 900 |

**Все пункты выполнены: 4 балла.**