package sieve

import "math"

type Algorithm struct {
	sieve []bool
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a *Algorithm) Calculate(number int64) int64 {
	if number <= 1 {
		return 0
	}

	count := int64(0)
	a.sieve = make([]bool, number + 1)
	limit := int64(math.Trunc(math.Sqrt(float64(number))))

	for i := int64(2); i <= number; i++ {
		if a.sieve[i] {
			continue
		}

		count++

		if i > limit {
			continue
		}

		for j := i * i; j <= number; j += i {
			a.sieve[j] = true
		}
	}

	return count
}
