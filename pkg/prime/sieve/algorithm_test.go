package sieve

import (
	"gitlab.com/gzavodov/dsa/pkg/prime/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestSieveAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация поиска количества простых чисел в диапазоне от 1 до N решето Эратосфена O(N log log N)",
		Task: &common.CalculationTask{
			Algorithm:         NewAlgorithm(),
			DataDirectoryPath: "../../../testify/6.Primes",
		},
	}

	runner.TestAll(t)
}
