package brute_force_e

import "math"

type Algorithm struct{}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a *Algorithm) Calculate(number int64) int64 {
	count := int64(0)
	for i := int64(2); i <= number; i++ {
		if a.IsPrimeNumber(i) {
			count++
		}
	}

	return count
}

func (a *Algorithm) IsPrimeNumber(number int64) bool {
	if number == 2 {
		return true
	}

	if number%2 == 0 {
		return false
	}

	limit := int64(math.Trunc(math.Sqrt(float64(number))))
	for i := int64(3); i <= limit; i += 2 {
		if number%i == 0 {
			return false
		}
	}

	return true
}
