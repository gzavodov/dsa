package sieve_linear

type Algorithm struct {
	sieve []bool
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a *Algorithm) Calculate(number int64) int64 {
	if number <= 1 {
		return 0
	}

	lp := make([]int64, number + 1)
	pr := make([]int64, 0, number + 1)

	for i := int64(2); i <= number; i++ {
		if lp[i] == 0 {
			lp[i] = i
			pr = append(pr, i)
		}

		for _, p := range pr {
			if !(p <= lp[i] && p * i <= number) {
				break
			}

			lp[p * i] = p
		}
	}

	return int64(len(pr))
}
