package brute_force_c

type Algorithm struct {}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a *Algorithm) Calculate(number int64) int64 {
	count := int64(0)
	for i := int64(2); i <= number; i++ {
		if a.IsPrimeNumber(i) {
			count++
		}
	}

	return count
}

func (a *Algorithm) IsPrimeNumber(number int64) bool {
	if number == 2 {
		return true
	}

	for i, limit := int64(2), number / 2; i <= limit; i++ {
		if number% i == 0 {
			return false
		}
	}

	return true
}