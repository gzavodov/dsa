package common

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"strconv"
	"time"
)

type CalculationTask struct {
	Algorithm         Algorithm
	DataDirectoryPath string
}

func (t *CalculationTask) Run(in string) *test.Result {
	n, err := strconv.ParseInt(in, 10, 64)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(err)
	}

	start := time.Now()
	value := t.Algorithm.Calculate(n)
	finish := time.Now()

	str := strconv.FormatInt(value, 10)

	return test.NewResult().
		WithSummary(
			fmt.Sprintf(
				"%s: %s",
				strconv.FormatInt(n, 10),
				str,
			),
		).
		OnComplete(start, finish, str)
}

func (t *CalculationTask) GetDataSource() test.DataSource {
	return &test.FileDataSource{DataDirectoryPath: t.DataDirectoryPath}
}
