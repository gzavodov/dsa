package common

type Algorithm interface {
	Calculate(number int64) int64
}
