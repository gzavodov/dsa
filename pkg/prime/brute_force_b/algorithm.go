package brute_force_b

type Algorithm struct {}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a *Algorithm) Calculate(number int64) int64 {
	count := int64(0)
	for i := int64(2); i <= number; i++ {
		if a.IsPrimeNumber(i) {
			count++
		}
	}

	return count
}

func (a *Algorithm) IsPrimeNumber(number int64) bool {
	if number == 2 {
		return true
	}

	for i := int64(2); i < number; i++ {
		if number% i == 0 {
			return false
		}
	}

	return true
}