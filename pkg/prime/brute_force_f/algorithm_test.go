package brute_force_f

import (
	"gitlab.com/gzavodov/dsa/pkg/prime/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestBruteForceFAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация поиска количества простых чисел в диапазоне от 1 до N перебором (вариант F)",
		Task: &common.CalculationTask{
			Algorithm:         NewAlgorithm(),
			DataDirectoryPath: "../../../testify/6.Primes",
		},
	}

	// Последние тесты (1 000 000 000, 123 456 789) отключены из-за очень долгого времени работы
	runner.Test(t, -2)
}
