package brute_force_f

import "math"

type Algorithm struct {
	primes []int64
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a *Algorithm) Calculate(number int64) int64 {
	if number <= 1 {
		return 0
	}
	
	a.primes = make([]int64, 0, number)
	count := int64(0)

	a.primes = append(a.primes, 2)
	count++

	for i := int64(3); i <= number; i += 2 {
		if a.IsPrimeNumber(i) {
			a.primes = append(a.primes, i)
			count++
		}
	}

	return count
}

func (a *Algorithm) IsPrimeNumber(number int64) bool {
	limit := int64(math.Trunc(math.Sqrt(float64(number))))
	for _, prime := range a.primes {
		if prime > limit {
			break
		}

		if number%prime == 0 {
			return false
		}
	}

	return true
}
