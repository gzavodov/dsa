package sort

import (
	"errors"
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"strconv"
	"strings"
	"time"
)

type Task struct {
	Algorithm         Algorithm
	DataDirectoryPath string
}

func (t *Task) Run(in string) *test.Result {
	parts := strings.SplitN(in, "\r\n", 2)
	if len(parts) < 2 {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(errors.New("не удалось разобрать входную строку"))
	}

	incomeSlugs := strings.Split(parts[1], " ")
	length := len(incomeSlugs)
	numbers := make([]int, length)

	for i, slug := range incomeSlugs {
		num, err := strconv.Atoi(slug)
		if err != nil {
			return test.NewResult().
				WithSummary(fmt.Sprintf("входная строка: \"%s\"", slug)).
				OnError(errors.New("не удалось конвертировать в число"))
		}

		numbers[i] = num

	}

	start := time.Now()
	t.Algorithm.Sort(numbers)
	finish := time.Now()

	outcomeSlugs := make([]string, length)
	for i, number := range numbers {
		outcomeSlugs[i] = strconv.Itoa(number)
	}

	return test.NewResult().
		WithSummary(
			fmt.Sprintf("Количество эл-тов: %d", length),
		).
		OnComplete(start, finish, strings.Join(outcomeSlugs, " "))
}

func (t *Task) GetDataSource() test.DataSource {
	return &test.FileDataSource{DataDirectoryPath: t.DataDirectoryPath}
}