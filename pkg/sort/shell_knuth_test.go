package sort

import (
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestShellKnuthSort(t *testing.T) {
	randomRunner := test.Runner{
		Caption: "Массив случайных чисел (random). Сортировка методом Д. Шелла с генерацией приращений методом Д. Кнута",
		Task: &Task{
			Algorithm: Shell{IncrementGenerator: KnuthIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/0.random",
		},
		HideResults: true,
	}
	randomRunner.Test(t, 0)

	digitsRunner := test.Runner{
		Caption: "Массив случайных чисел (digits). Сортировка методом Д. Шелла с генерацией приращений методом Д. Кнута",
		Task: &Task{
			Algorithm: Shell{IncrementGenerator: KnuthIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/1.digits",
		},
		HideResults: true,
	}
	digitsRunner.Test(t, 0)

	sortedRunner := test.Runner{
		Caption: "Массив отсортированный на 99 %. Сортировка методом Д. Шелла с генерацией приращений методом Д. Кнута",
		Task: &Task{
			Algorithm: Shell{IncrementGenerator: KnuthIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/2.sorted",
		},
		HideResults: true,
	}
	sortedRunner.Test(t, 0)

	reversedRunner := test.Runner{
		Caption: "Массив отсортированный в обратном порядке. Сортировка методом Д. Шелла с генерацией приращений методом Д. Кнута",
		Task: &Task{
			Algorithm: Shell{IncrementGenerator: KnuthIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/3.revers",
		},
		HideResults: true,
	}
	reversedRunner.Test(t, 0)
}
