package sort

import (
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestSelectionSort(t *testing.T) {
	runner := test.Runner{
		Caption: "Массив случайных чисел (random). Сортировка методом выбора",
		Task: &Task{
			Algorithm:         Selection{},
			DataDirectoryPath: "../../testify/7.Sorting/0.random",
		},
		HideResults: true,
	}
	runner.Test(t, -2)

	digitsRunner := test.Runner{
		Caption: "Массив случайных чисел (digits). Сортировка методом выбора",
		Task: &Task{
			Algorithm:         Selection{},
			DataDirectoryPath: "../../testify/7.Sorting/1.digits",
		},
		HideResults: true,
	}
	digitsRunner.Test(t, -2)

	sortedRunner := test.Runner{
		Caption: "Массив отсортированный на 99 %. Сортировка методом выбора",
		Task: &Task{
			Algorithm:         Selection{},
			DataDirectoryPath: "../../testify/7.Sorting/2.sorted",
		},
		HideResults: true,
	}
	sortedRunner.Test(t, -2)

	reversedRunner := test.Runner{
		Caption: "Массив отсортированный в обратном порядке. Сортировка методом выбора",
		Task: &Task{
			Algorithm:         Selection{},
			DataDirectoryPath: "../../testify/7.Sorting/3.revers",
		},
		HideResults: true,
	}
	reversedRunner.Test(t, -2)
}
