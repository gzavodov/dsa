package sort

type Algorithm interface {
	Sort([]int)
}
