package sort

type Insertion struct {
}

func (s Insertion) Sort(numbers []int) {
	for i, length := 1, len(numbers); i < length; i++ {
		number := numbers[i]

		j := i - 1
		for {
			if !(j >= 0 && numbers[j] > number) {
				break
			}

			numbers[j+1] = numbers[j]
			j--
		}

		numbers[j+1] = number
	}
}

