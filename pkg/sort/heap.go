package sort

type Heap struct {
	Heapifier func(numbers []int, length int, root int)
}

func (s Heap) Sort(numbers []int) {
	length := len(numbers)

	//fmt.Printf("%+v\n", numbers)

	for i := (length - 1) / 2; i >= 0; i-- {
		s.Heapifier(numbers, length, i)
	}

	for i := length - 1; i >= 0; i-- {
		numbers[0], numbers[i] = numbers[i], numbers[0]
		s.Heapifier(numbers, i, 0)
	}

	//fmt.Printf("%+v\n", numbers)
}

func HeapifyClassic(numbers []int, length int, root int) {
	largest, left, right := root, 2*root+1, 2*root+2

	if left < length && numbers[left] > numbers[largest] {
		largest = left
	}

	if right < length && numbers[right] > numbers[largest] {
		largest = right
	}

	if largest != root {
		numbers[root], numbers[largest] = numbers[largest], numbers[root]

		HeapifyClassic(numbers, length, largest)
	}
}

func HeapifyStdLib(numbers []int, length int, root int) {
	for {
		child := 2*root + 1

		if child >= length {
			break
		}

		if child+1 < length && numbers[child+1] > numbers[child] {
			child++
		}

		if numbers[root] >= numbers[child] {
			break
		}

		numbers[root], numbers[child] = numbers[child], numbers[root]
		root = child
	}
}
