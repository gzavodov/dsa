package sort

/*
 * Compare two slices to check whether their contents are equal.
 */
func areSlicesEqual(a, b []int) bool {
	// Check whether the two slices are of the same size.
	if len(a) != len(b) {
		return false
	}

	for i, length := 0, len(a); i < length; i++ {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
