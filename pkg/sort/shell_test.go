package sort

import (
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestShellSort(t *testing.T) {
	randomRunner := test.Runner{
		Caption: "Массив случайных чисел (random). Сортировка методом Д. Шелла с генерацией приращений методом Д. Шелла",
		Task: &Task{
			Algorithm:         Shell{IncrementGenerator: ShellIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/0.random",
		},
		HideResults: true,
	}
	randomRunner.TestAll(t)

	digitsRunner := test.Runner{
		Caption: "Массив случайных чисел (digits). Сортировка методом Д. Шелла с генерацией приращений методом Д. Шелла",
		Task: &Task{
			Algorithm:         Shell{IncrementGenerator: ShellIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/1.digits",
		},
		HideResults: true,
	}
	digitsRunner.TestAll(t)

	sortedRunner := test.Runner{
		Caption: "Массив отсортированный на 99 %. Сортировка методом Д. Шелла с генерацией приращений методом Д. Шелла",
		Task: &Task{
			Algorithm:         Shell{IncrementGenerator: ShellIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/2.sorted",
		},
		HideResults: true,
	}
	sortedRunner.TestAll(t)

	reversedRunner := test.Runner{
		Caption: "Массив отсортированный в обратном порядке. Сортировка методом Д. Шелла с генерацией приращений методом Д. Шелла",
		Task: &Task{
			Algorithm:         Shell{IncrementGenerator: ShellIncrementGenerator},
			DataDirectoryPath: "../../testify/7.Sorting/3.revers",
		},
		HideResults: true,
	}
	reversedRunner.TestAll(t)
}
