package sort

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestHeapSortClassicSimple(t *testing.T) {
	expected := []int{1, 2, 3, 9, 11, 13, 17, 25, 57, 90}
	working := []int{11, 2, 9, 13, 57, 25, 17, 1, 90, 3}

	fmt.Printf("IN:  %+v\n", working)
	Heap{Heapifier: HeapifyClassic}.Sort(working)
	fmt.Printf("OUT: %+v\n", working)

	if !areSlicesEqual(working, expected) {
		t.Errorf("expected result: %+v, but received: %+v", expected, working)
	}
}

func TestHeapSortClassic(t *testing.T) {
	randomRunner := test.Runner{
		Caption: "Массив случайных чисел (random). Пирамидальная сортировка (рекурсивная реализация)",
		Task: &Task{
			Algorithm:         Heap{Heapifier: HeapifyClassic},
			DataDirectoryPath: "../../testify/7.Sorting/0.random",
		},
		HideResults: true,
	}
	randomRunner.TestAll(t)

	digitsRunner := test.Runner{
		Caption: "Массив случайных чисел (digits). Пирамидальная сортировка (рекурсивная реализация)",
		Task: &Task{
			Algorithm:         Heap{Heapifier: HeapifyClassic},
			DataDirectoryPath: "../../testify/7.Sorting/1.digits",
		},
		HideResults: true,
	}
	digitsRunner.TestAll(t)

	sortedRunner := test.Runner{
		Caption: "Массив отсортированный на 99 %. Пирамидальная сортировка (рекурсивная реализация)",
		Task: &Task{
			Algorithm:         Heap{Heapifier: HeapifyClassic},
			DataDirectoryPath: "../../testify/7.Sorting/2.sorted",
		},
		HideResults: true,
	}
	sortedRunner.TestAll(t)

	reversedRunner := test.Runner{
		Caption: "Массив отсортированный в обратном порядке. Пирамидальная сортировка (рекурсивная реализация)",
		Task: &Task{
			Algorithm:         Heap{Heapifier: HeapifyClassic},
			DataDirectoryPath: "../../testify/7.Sorting/3.revers",
		},
		HideResults: true,
	}
	reversedRunner.TestAll(t)
}
