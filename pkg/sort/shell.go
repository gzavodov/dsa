package sort

type Shell struct {
	IncrementGenerator func(size int) []int
}

func (s Shell) Sort(numbers []int) {
	length := len(numbers)
	increments := s.IncrementGenerator(length)

	stepCount := 0

	for i := 0; i < len(increments); i++ {
		inc := increments[i]
		for j := 0; j < (length - inc); j++ {
			k := j + inc
			number := numbers[k]
			for {
				if !((k-inc) >= 0 && numbers[k-inc] > number) {
					break
				}

				stepCount++
				numbers[k] = numbers[k-inc]
				k -= inc
			}

			numbers[k] = number
		}
	}

	//fmt.Printf("====Size: %d  Step Count: %d ===\n", length, stepCount)
}

func ShellIncrementGenerator(size int) []int {
	var increments []int
	for i := size / 2; i >= 1; i /= 2 {
		increments = append(increments, i)
	}
	return increments
}

func KnuthIncrementGenerator(size int) []int {
	var increments []int
	for i := 1; i < size; i = 3*i + 1 {
		increments = append(increments, i)
	}
	return reverse(increments)
}

func SedgewickIncrementGenerator(size int) []int {
	var increments []int
	p1, p2, p3, i := 1, 1, 1, 0

	for {
		var inc int
		if i%2 > 0 {
			inc = 8*p1 - 6*p2 + 1
			p1 *= 2
		} else {
			inc = 9*p1 - 9*p3 + 1
			p1 *= 2
			p2 *= 2
			p3 *= 2
		}

		increments = append(increments, inc)
		if 3*increments[i] >= size {
			break
		}

		i++
	}

	return reverse(increments)
}

func reverse(numbers []int) []int {
	newNumbers := make([]int, len(numbers))
	for i, j := 0, len(numbers)-1; i <= j; i, j = i+1, j-1 {
		newNumbers[i], newNumbers[j] = numbers[j], numbers[i]
	}
	return newNumbers
}
