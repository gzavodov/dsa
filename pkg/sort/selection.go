package sort

type Selection struct {
}

func (s Selection) Sort(numbers []int)  {
	length := len(numbers)

	for i := 0; i < length; i++ {
		minimal := i

		for j := i+1; j < length; j++ {
			if numbers[j] < numbers[minimal] {
				minimal = j
			}
		}

		if minimal != i {
			numbers[i], numbers[minimal] = numbers[minimal], numbers[i]
		}
	}
}