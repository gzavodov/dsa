package dynamic_array

type SingleArray struct {
	array []interface{}
}

func NewSingleArray() *SingleArray {
	return &SingleArray{
		array: make([]interface{}, 0),
	}
}

func (a *SingleArray) Size() int {
	return len(a.array)
}

func (a *SingleArray) Get(index int) interface{} {
	if index < 0 || index >= len(a.array) {
		return nil
	}

	return a.array[index]
}

func (a *SingleArray) Add(item interface{}) {
	array := make([]interface{}, len(a.array)+1)
	copy(array, a.array)
	a.array = array

	a.array[len(a.array)-1] = item
}

func (a *SingleArray) Insert(item interface{}, index int) {
	if index < 0 {
		return
	}

	if index >= len(a.array) {
		a.Add(item)
		return
	}

	array := make([]interface{}, len(a.array)+1)
	copy(array[:index], a.array[:index])
	array[index] = item
	copy(array[index+1:], a.array[index:])

	a.array = array
}

func (a *SingleArray) Remove(index int) {
	if index < 0 || index >= len(a.array) {
		return
	}

	array := make([]interface{}, len(a.array)-1)
	copy(array[:index], a.array[:index])
	if len(array) > index {
		copy(array[index:], a.array[index+1:])
	}

	a.array = array
}
