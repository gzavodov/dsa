package dynamic_array

import (
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestMatrixArrayPerformance(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация динамического массива MatrixArray",
		Task:    &BenchmarkTask{
			Array: NewMatrixArray(1000),
			Limits: []int{1000, 10000, 100000, 1000000, 10000000},
		},
	}

	runner.TestAll(t)
}

