package dynamic_array

type StandardArray struct {
	array []interface{}
}

func NewStandardArray(capacity int) *StandardArray {
	return &StandardArray{
		array: make([]interface{}, 0, capacity),
	}
}

func (a *StandardArray) Size() int {
	return len(a.array)
}

func (a *StandardArray) Get(index int) interface{} {
	if index < 0 || index >= len(a.array) {
		return nil
	}

	return a.array[index]
}

func (a *StandardArray) Add(item interface{}) {
	a.array = append(a.array, item)
}

func (a *StandardArray) Insert(item interface{}, index int) {
	if index < 0 {
		return
	}

	if index >= len(a.array) {
		a.array = append(a.array, item)
		return
	}

	a.array = append(a.array, nil)
	copy(a.array[index+1:], a.array[index:])
	a.array[index] = item
}

func (a *StandardArray) Remove(index int) {
	if index < 0 || index >= len(a.array) {
		return
	}

	a.array = append(a.array[:index], a.array[index+1:]...)
}
