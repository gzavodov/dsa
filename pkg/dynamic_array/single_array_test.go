package dynamic_array

import (
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestSingleArrayPerformance(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация динамического массива SingleArray (без резервирования места в низлежащем массиве)",
		Task:    &BenchmarkTask{
			Array: NewSingleArray(),
			Limits: []int{1000, 10000},
		},
	}

	runner.TestAll(t)
}

