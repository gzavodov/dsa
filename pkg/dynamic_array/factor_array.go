package dynamic_array

type FactorArray struct {
	array []interface{}

	length         int
	capacity       int
	capacityFactor int
}

func NewFactorArray(capacityFactor int) *FactorArray {
	return &FactorArray{
		array:          make([]interface{}, capacityFactor, capacityFactor),
		length:         0,
		capacity:       capacityFactor,
		capacityFactor: capacityFactor,
	}
}

func (a *FactorArray) Size() int {
	return a.length
}

func (a *FactorArray) Get(index int) interface{} {
	if index < 0 || index >= a.length {
		return nil
	}

	return a.array[index]
}

func (a *FactorArray) Add(item interface{}) {
	if a.length == a.capacity {
		capacity := a.capacity * a.capacityFactor + 1
		array := make([]interface{}, capacity, capacity)
		copy(array, a.array)

		a.array = array
		a.capacity = capacity
	}

	a.array[a.length] = item
	a.length++
}

func (a *FactorArray) Insert(item interface{}, index int) {
	if index < 0 {
		return
	}

	if index >= a.length {
		a.Add(item)
		return
	}

	if a.length < a.capacity {
		copy(a.array[index+1:], a.array[index:])
		a.array[index] = item
		a.length++
	} else {
		capacity := a.capacity * a.capacityFactor + 1
		array := make([]interface{}, capacity, capacity)
		copy(array[:index], a.array[:index])
		array[index] = item
		copy(array[index+1:], a.array[index:])
		a.length++

		a.array = array
		a.capacity = capacity
	}
}

func (a *FactorArray) Remove(index int) {
	if index < 0 || index >= a.length {
		return
	}

	if index < a.length-1 {
		copy(a.array[index:], a.array[index+1:])
	}
	a.length--
}
