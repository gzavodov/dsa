package dynamic_array

type VectorArray struct {
	array []interface{}

	length          int
	capacity          int
	capacityIncrement int
}

func NewVectorArray(capacityIncrement int) *VectorArray {
	return &VectorArray{
		array:             make([]interface{}, capacityIncrement, capacityIncrement),
		length:            0,
		capacity:          capacityIncrement,
		capacityIncrement: capacityIncrement,
	}
}

func (a *VectorArray) Size() int {
	return a.length
}

func (a *VectorArray) Get(index int) interface{} {
	if index < 0 || index >= a.length {
		return nil
	}

	return a.array[index]
}

func (a *VectorArray) Add(item interface{}) {
	if a.length == a.capacity {
		capacity := a.capacity + a.capacityIncrement
		array := make([]interface{}, capacity, capacity)
		copy(array, a.array)

		a.array = array
		a.capacity = capacity
	}

	a.array[a.length] = item
	a.length++
}

func (a *VectorArray) Insert(item interface{}, index int) {
	if index < 0 {
		return
	}

	if index >= a.length {
		a.Add(item)
		return
	}

	if a.length == a.capacity {
		capacity := a.capacity + a.capacityIncrement
		array := make([]interface{}, capacity, capacity)
		copy(array[:index], a.array[:index])
		array[index] = item
		copy(array[index+1:], a.array[index:])
		a.length++

		a.array = array
		a.capacity = capacity
	} else {
		copy(a.array[index+1:], a.array[index:])
		a.array[index] = item
		a.length++
	}

}

func (a *VectorArray) Remove(index int) {
	if index < 0 || index >= a.length {
		return
	}

	if index < a.length-1 {
		copy(a.array[index:], a.array[index+1:])
	}
	a.length--
}
