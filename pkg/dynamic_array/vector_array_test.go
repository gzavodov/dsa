package dynamic_array

import (
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestVectorArrayPerformance(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация динамического массива VectorArray",
		Task:    &BenchmarkTask{
			Array: NewVectorArray(1000),
			Limits: []int{1000, 10000, 100000, 1000000},
		},
	}

	runner.TestAll(t)
}

