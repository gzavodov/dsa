package dynamic_array

type MatrixArray struct {
	primary [][]interface{}

	primaryCapacity       int
	primaryLength         int

	secondaryCapacity int
	secondaryLength   int

	capacity int
	length   int
}

func NewMatrixArray(secondaryCapacity int) *MatrixArray {
	primaryCapacity := 1
	primary := make([][]interface{}, primaryCapacity, primaryCapacity)
	for i := 0; i < primaryCapacity; i++ {
		primary[i] = make([]interface{}, secondaryCapacity, secondaryCapacity)
	}

	return &MatrixArray{
		primary:               primary,
		primaryCapacity:       primaryCapacity,
		primaryLength:         primaryCapacity,
		secondaryCapacity:     secondaryCapacity,
		secondaryLength:       0,
		capacity:              primaryCapacity * secondaryCapacity,
		length:                0,
	}
}

func (a *MatrixArray) Size() int {
	return a.length
}

func (a *MatrixArray) Get(index int) interface{} {
	if index < 0 || index >= a.length {
		return nil
	}

	primaryIndex := index / a.secondaryCapacity
	if primaryIndex >= a.primaryLength {
		return nil
	}

	secondary := a.primary[primaryIndex]
	secondaryIndex := index % a.secondaryCapacity

	secondaryLastIndex := a.secondaryLength - 1
	if primaryIndex < a.primaryLength-1 {
		secondaryLastIndex = a.secondaryCapacity - 1
	}

	if secondaryIndex >= secondaryLastIndex {
		return nil
	}

	return secondary[secondaryIndex]
}

func (a *MatrixArray) Add(item interface{}) {
	a.extend()

	secondary := a.primary[a.primaryLength-1]
	secondaryIndex := a.length % a.secondaryCapacity
	secondary[secondaryIndex] = item
	a.secondaryLength++
	a.length++
}

func (a *MatrixArray) Insert(item interface{}, index int) {
	if index < 0 {
		return
	}

	if index >= a.length {
		a.Add(item)
		return
	}

	a.extend()

	primaryIndex := index / a.secondaryCapacity
	secondaryIndex := index % a.secondaryCapacity
	secondary := a.primary[primaryIndex]

	secondaryLastIndex := a.secondaryLength - 1
	if primaryIndex < a.primaryLength-1 {
		secondaryLastIndex = a.secondaryCapacity - 1
	}

	var secondaryTail interface{}
	if secondaryLastIndex >= 0 {
		secondaryTail = secondary[secondaryLastIndex]
	}

	copy(secondary[secondaryIndex+1:], secondary[secondaryIndex:])
	secondary[secondaryIndex] = item

	if secondaryTail != nil {
		shifting := secondaryTail
		for i := primaryIndex + 1; i < a.primaryLength; i++ {
			secondary = a.primary[i]

			secondaryLastIndex = a.secondaryLength - 1
			if i < a.primaryLength-1 {
				secondaryLastIndex = a.secondaryCapacity - 1
			}

			secondaryTail = nil
			if secondaryLastIndex >= 0 {
				secondaryTail = secondary[secondaryLastIndex]
				copy(secondary[1:], secondary)
			}

			secondary[0] = shifting
			if secondaryTail == nil {
				break
			}

			shifting = secondaryTail
		}
	}

	a.secondaryLength++
	a.length++
}

func (a *MatrixArray) Remove(index int) {
	if index < 0 || index >= a.length {
		return
	}

	primaryIndex := index / a.secondaryCapacity
	secondaryIndex := index % a.secondaryCapacity

	secondary := a.primary[primaryIndex]

	secondaryLastIndex := a.secondaryLength - 1
	if primaryIndex < a.primaryLength-1 {
		secondaryLastIndex = a.secondaryCapacity - 1
	}

	if secondaryIndex < secondaryLastIndex {
		copy(secondary[secondaryIndex:], secondary[secondaryIndex+1:])
	}

	secondary[secondaryLastIndex] = nil
	a.secondaryLength--
	a.length--
}

func (a *MatrixArray) extend() {
	if a.length < a.capacity {
		return
	}

	primaryCapacity := a.primaryCapacity + 1
	primary := make([][]interface{}, primaryCapacity, primaryCapacity)

	copy(primary, a.primary)

	primaryLength := a.primaryLength
	primary[primaryLength] = make([]interface{}, a.secondaryCapacity, a.secondaryCapacity)
	primaryLength++

	a.primary = primary
	a.primaryCapacity = primaryCapacity
	a.primaryLength = primaryLength
	a.secondaryLength = 0
	a.capacity = a.primaryCapacity * a.secondaryCapacity
}
