package dynamic_array

import (
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestStandardArrayPerformance(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация динамического массива StandardArray",
		Task:    &BenchmarkTask{
			Array: NewStandardArray(1000),
			Limits: []int{1000, 10000, 100000, 1000000, 10000000, 100000000},
		},
	}

	runner.TestAll(t)
}
