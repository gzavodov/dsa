package dynamic_array

type Array interface {

	Add(interface{})
	Size()int
	Get(int) interface{}
	Insert(interface{}, int)
	Remove(int)
}
