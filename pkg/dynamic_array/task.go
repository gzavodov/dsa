package dynamic_array

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"strconv"
)

type BenchmarkTask struct {
	Array Array
	Limits []int

	dataSource *test.InMemoryDataSource
}

func (t *BenchmarkTask) GetDataSource() test.DataSource {
	if t.dataSource != nil {
		return t.dataSource
	}

	data := make([]test.TaskData, len(t.Limits))
	for i, limit := range t.Limits {
		data[i].Income = strconv.Itoa(limit)
	}

	t.dataSource = &test.InMemoryDataSource{Data: data, EnableOutcomeCheck: false}

	return t.dataSource
}

func (t *BenchmarkTask) Run(in string) *test.Result {
	limit, err := strconv.ParseInt(in, 10, 32)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(err)
	}

	result := test.NewResult().OnStart()
	for i := int64(0); i < limit; i++ {
		t.Array.Add(i)
	}

	return result.OnFinish("OK").
		WithSummary(fmt.Sprintf("Кол-во элементов: %d", limit))
}