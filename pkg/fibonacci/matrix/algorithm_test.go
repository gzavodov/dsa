package matrix

import (
	"gitlab.com/gzavodov/dsa/pkg/fibonacci/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestMatrixAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация поиска N-ого числа Фибоначчи через умножение матриц",
		Task: &common.CalculationTask{
			Algorithm:         NewAlgorithm(),
			DataDirectoryPath: "../../../testify/5.Fibo",
		},
		HideErrors:  true,
		HideResults: true,
	}

	runner.TestAll(t)
}
