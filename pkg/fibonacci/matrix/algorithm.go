package matrix

import "math/big"

type Algorithm struct {
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

var (
	zero = big.NewInt(0)
	one  = big.NewInt(1)
	two  = big.NewInt(2)
)

var initial = [2][2]*big.Int{{one, one}, {one, zero}}

func (a *Algorithm) Multiplicate(left, right [2][2]*big.Int) [2][2]*big.Int {
	return [2][2]*big.Int{
		{
			new(big.Int).Add(new(big.Int).Mul(left[0][0], right[0][0]), new(big.Int).Mul(left[0][1], right[1][0])),
			new(big.Int).Add(new(big.Int).Mul(left[0][0], right[0][1]), new(big.Int).Mul(left[0][1], right[1][1])),
		},
		{
			new(big.Int).Add(new(big.Int).Mul(left[1][0], right[0][0]), new(big.Int).Mul(left[1][1], right[1][0])),
			new(big.Int).Add(new(big.Int).Mul(left[1][0], right[0][1]), new(big.Int).Mul(left[1][1], right[1][1])),
		},
	}
}

func (a *Algorithm) Pow(base [2][2]*big.Int, exponent *big.Int) [2][2]*big.Int {
	if exponent.Cmp(zero) == 0 {
		return [2][2]*big.Int{{one, zero}, {zero, one}}
	}

	if exponent.Cmp(one) == 0 {
		return base
	}

	result := base

	currentExponent := new(big.Int).Set(one)
	for {
		if exponent.Cmp(new(big.Int).Mul(currentExponent, two)) < 0 {
			break
		}

		result = a.Multiplicate(result, result)
		currentExponent.Mul(currentExponent, two)
	}

	diffExponent := new(big.Int).Sub(exponent, currentExponent)
	if diffExponent.Cmp(zero) > 0 {
		return a.Multiplicate(result, a.Pow(base, diffExponent))
	}

	return result
}

func (a *Algorithm) Calculate(n *big.Int) *big.Int {
	if n.Cmp(one) <= 0 {
		return n
	}

	if n.Cmp(two) == 0 {
		return initial[0][0]
	}

	return a.Pow(initial, new(big.Int).Sub(n, one))[0][0]
}
