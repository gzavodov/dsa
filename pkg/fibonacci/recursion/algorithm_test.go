package recursion

import (
	"gitlab.com/gzavodov/dsa/pkg/fibonacci/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestRecursionAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация поиска N-ого числа Фибоначчи через рекурсию",
		Task: &common.CalculationTask{
			Algorithm:         NewAlgorithm(),
			DataDirectoryPath: "../../../testify/5.Fibo",
		},
		HideErrors:  true,
		HideResults: true,
	}

	// Пропускаем тесты для чисел 1000000 и 10000000 из-за превышения максимального размера стека 1Гб для 64-битных систем
	runner.Test(t, -2)
}
