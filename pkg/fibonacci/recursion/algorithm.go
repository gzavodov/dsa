package recursion

import (
	"math/big"
)

type Algorithm struct {
	cache map[string]*big.Int
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{cache: make(map[string]*big.Int)}
}
var one = big.NewInt(1)
func (a *Algorithm) Calculate(n *big.Int) *big.Int {
	if n.Cmp(one) <= 0 {
		return n
	}

	key := n.Text(10)
	if result, ok := a.cache[key]; ok {
		return result
	}

	result := add(
		a.Calculate(sub(n, big.NewInt(2))),
		a.Calculate(sub(n, big.NewInt(1))),
	)

	a.cache[key] = result

	return result
}

func add(x, y *big.Int) *big.Int {
	return new(big.Int).Add(x, y)
}

func sub(x, y *big.Int) *big.Int {
	return new(big.Int).Sub(x, y)
}