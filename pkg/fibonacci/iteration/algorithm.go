package iteration

import (
	"math/big"
)

type Algorithm struct {
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

func (a *Algorithm) Calculate(n *big.Int) *big.Int {
	if n.Cmp(big.NewInt(1)) <= 0 {
		return n
	}

	x := big.NewInt(0)
	y := big.NewInt(1)
	z := big.NewInt(0)

	for i := big.NewInt(2); i.Cmp(n) <= 0; i.Add(i, big.NewInt(1)) {
		z.Add(x, y)

		x.Set(y)
		y.Set(z)
	}

	return z
}
