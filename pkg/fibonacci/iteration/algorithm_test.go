package iteration

import (
	"gitlab.com/gzavodov/dsa/pkg/fibonacci/common"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"testing"
)

func TestIterationAlgorithm(t *testing.T) {
	runner := test.Runner{
		Caption: "Реализация поиска N-ого числа Фибоначчи через итерации",
		Task: &common.CalculationTask{
			Algorithm:         NewAlgorithm(),
			DataDirectoryPath: "../../../testify/5.Fibo",
		},
		HideErrors:  true,
		HideResults: true,
	}

	runner.TestAll(t)
}
