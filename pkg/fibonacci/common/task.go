package common

import (
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/test"
	"math/big"
	"strconv"
)

type CalculationTask struct {
	Algorithm         Algorithm
	DataDirectoryPath string
}


func (t *CalculationTask) Run(in string) *test.Result {
	number, err := strconv.ParseInt(in, 10, 64)
	if err != nil {
		return test.NewResult().
			WithSummary(fmt.Sprintf("входная строка: \"%s\"", in)).
			OnError(err)
	}

	result := test.NewResult().OnStart()
	value := t.Algorithm.Calculate(big.NewInt(number))

	return result.OnFinish(value.Text(10)).WithSummary(in)
}

func (t *CalculationTask) GetDataSource() test.DataSource {
	return &test.FileDataSource{DataDirectoryPath: t.DataDirectoryPath}
}
