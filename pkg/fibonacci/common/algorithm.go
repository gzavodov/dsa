package common

import "math/big"

type Algorithm interface {
	Calculate(*big.Int ) *big.Int
}
