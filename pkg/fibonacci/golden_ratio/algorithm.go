package golden_ratio

import (
	"gitlab.com/gzavodov/dsa/pkg/power/binary"
	"math/big"
)

const precision = 8388608

type Algorithm struct {
	fi *big.Float
}

func NewAlgorithm() *Algorithm {
	return &Algorithm{}
}

var one = big.NewInt(1)
var sqrt5 = new(big.Float).Sqrt(big.NewFloat(5).SetPrec(precision))

func (a *Algorithm) GetFi() *big.Float {
	if a.fi == nil {
		a.fi = new(big.Float).Add(big.NewFloat(1).SetPrec(precision), sqrt5)
		a.fi.Quo(a.fi, big.NewFloat(2).SetPrec(precision))
	}

	return a.fi
}

func (a *Algorithm) Calculate(n *big.Int) *big.Int {
	if n.Cmp(one) <= 0 {
		return n
	}

	//fi^n / sqrt(5) + 0.5
	result := binary.NewAlgorithm().CalculateBigFloat(a.GetFi(), n.Int64(), precision)
	result.Quo(result, sqrt5)
	result.Add(result, big.NewFloat(0.5))

	intResult, _ := result.Int(nil)
	return intResult
}