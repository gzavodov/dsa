package main

import (
	"flag"
	"fmt"
	"gitlab.com/gzavodov/dsa/pkg/array/deduplicator"
)

func main() {
	length := flag.Int("length", 10, "Length of duplicates array")
	limit := flag.Int("limit", 2, "Maximum count of duplicates for number")

	flag.Parse()

	duplicates, sequence := deduplicator.Generate[int64](*length, *limit)
	fmt.Printf("Duplicates: %d\n", len(duplicates))
	fmt.Printf("%v\n", duplicates)
	fmt.Printf("Sequence: %d\n", len(sequence))
	fmt.Printf("%v\n", sequence)
}
